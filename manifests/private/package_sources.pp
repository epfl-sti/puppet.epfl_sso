class epfl_sso::private::package_sources inherits epfl_sso::private::params {
  if ($osfamily == "RedHat") {
    class { "epel": }
    Yumrepo["epel"] ->Package<| |>

    ## bsh and its dependencies need these.
    $_optional_rpms_depot_suffix = "${osmajrelease}-${redhat_flavor}-optional-rpms"
    case $osfullrelease {
      "7.9": {
        if ($::operatingsystem == "RedHat") {
          $_optional_rpms_depot = "rhel-${_optional_rpms_depot_suffix}"
          exec { "subscription-manager repos --enable ${_optional_rpms_depot}":
            path => $::path,
            unless => "yum-config-manager ${_optional_rpms_depot} |grep 'enabled = True'"
          } -> Package<| |>
        } else {
          yumrepo { "centos-${_optional_rpms_depot_suffix}":
            enabled => true
          } -> Package<| |>
        }
      }
      "8": {
        exec { "dnf config-manager --set-enabled PowerTools":
          path => $::path,
          unless => "grep 'enabled=1' /etc/yum.repos.d/CentOS-PowerTools.repo"
        } -> Package<| |>
      }
    }
  }
}
