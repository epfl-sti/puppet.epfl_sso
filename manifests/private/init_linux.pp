# Main manifest for the Linux platform
# The Darwin entry point is so small it is folded into ../init.pp instead
class epfl_sso::private::init_linux(
  $allowed_users_and_groups,
  $manage_nsswitch_netgroup,
  $enable_mkhomedir,
  $ad_automount_home,
  $auth_source,
  $directory_source,
  $needs_nscd,
  $ldap_conf_path,
  $ad_servers,
  $ad_server_base_dn,
  $epflca_cert_url,
  $epflca_is_trusted,
  $ad_krb5_security_factor,
  $join_domain,
  $renew_domain_credentials,
  $sshd_gssapi_auth,
  $debug_gssd,
  $debug_sssd,
  $use_test_realm,
  $manage_smb_conf = $epfl_sso::private::params::manage_smb_conf,
) inherits epfl_sso::private::params {
  ensure_resource('class', 'quirks')

  class { "epfl_sso::private::package_sources": }
  class { "epfl_sso::private::login_shells": }
  if (str2bool($::is_lightdm_active)) {
    class { "epfl_sso::private::lightdm":  }
  }

  class { "epfl_sso::private::sss":
    auth_source              => $auth_source,
    directory_source         => $directory_source,
    use_test_realm           => $use_test_realm,
    ad_server_base_dn        => $use_test_realm ? {
        true    => $ad_server_test_base_dn,
        default => $ad_server_prod_base_dn
    },
    ad_automount_home        => $ad_automount_home,
    debug_sssd               => $debug_sssd,
    manage_nsswitch_netgroup => $manage_nsswitch_netgroup,
  }

  if ($needs_nscd) {
    package { "nscd":
      ensure => present
    }
  }

  # A properly configured clock is necessary for Kerberos:
  if ($osfamily == "RedHat" and
      $osmajrelease == "8"
     ) {
    package { "chrony":
      ensure => present
    }
  } else { 
    ensure_resource('class', 'ntp')
  }

  if ($allowed_users_and_groups != undef) {
    class { 'epfl_sso::private::access':
      directory_source         => $directory_source,
      allowed_users_and_groups => $allowed_users_and_groups
    }
  }

  if ($enable_mkhomedir) {
    $_mkhomedir_ensure = "present"
  } else {
    $_mkhomedir_ensure = "absent"
  }
  class { 'epfl_sso::private::mkhomedir':
    ensure => $_mkhomedir_ensure
  }

  epfl_sso::private::pam::module { "winbind":
    ensure => "absent"
  }

  anchor { "epfl_sso::samba_configured": }
  if ($manage_smb_conf) {
    class { "epfl_sso::private::smb_conf":
      use_test_realm           => $use_test_realm
    } ->
    Anchor["epfl_sso::samba_configured"]
  }

  if ($auth_source == "AD" or $directory_source == "AD") {
    class { "epfl_sso::private::epflca":
      epflca_cert_url         => $epflca_cert_url,
      epflca_is_trusted       => $epflca_is_trusted,
    }

    class { "epfl_sso::private::ad":
      # TODO: when $join_domain is undef and $directory_source is not "AD",
      # we shouldn't attempt to join the domain and hence, we shouldn't
      # attempt to install the prerequisites.
      join_domain              => $join_domain,
      renew_domain_credentials => $renew_domain_credentials,
      use_test_realm           => $use_test_realm
    }
  }

  if ($sshd_gssapi_auth != undef) {
    class { "epfl_sso::private::sshd":
      enable_gssapi => $sshd_gssapi_auth
    }
  }

  if ($ad_automount_home) {
    class { "epfl_sso::private::nfs":
      debug_gssd => $debug_gssd,
    }
    class { "epfl_sso::private::ad_automount_home": }
  }
}
