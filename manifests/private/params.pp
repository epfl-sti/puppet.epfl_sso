class epfl_sso::private::params {
  $krb5_realm = "INTRANET.EPFL.CH"
  $ad_krb5_best_security_factor = 56   # Best value as of 2020-01
  $ad_dns_domain = "intranet.epfl.ch"
  if ("${::epfl_krb5_resolved}" != "true") {
    fail("Couldn't resolve the Kerberos domain controller for ${krb5_realm} - Something might be wrong with your DNS server settings")
  }

  $epflca_cert_url = 'https://certauth.epfl.ch/epflca.cer'
  $epflca_is_trusted  = false                # root CA uses SHA-1 - See INC0329114
  $ad_cert_is_trusted = $epflca_is_trusted   # AD certificates are currently signed by EPFL's CA

  $ad_server_rrdns = "intranet.epfl.ch"
  # _rrdns above means "round-robin DNS." Unfortunately that doesn't
  # work in a number of cases, e.g. for LDAP/S that name is not in the
  # certificate, and the certificate is different on every replicated
  # AD server to begin with. Wherever possible, we enumerate all AD
  # servers instead.
  $ad_servers = [
    "ad1.${ad_dns_domain}",
    "ad2.${ad_dns_domain}",
    "ad3.${ad_dns_domain}",
    # In an email dated Sep 13, 2018, Laurent Durrer indicated that ad4
    # is not part of the production cluster.
    "ad5.${ad_dns_domain}",
    "ad6.${ad_dns_domain}"
  ]

  $krb5_test_realm = "EXTEST.EPFL.CH"
  $ad_test_dns_domain = "extest.epfl.ch"
  $ad_test_servers = [
    "tad1.${ad_test_dns_domain}",
    "tad2.${ad_test_dns_domain}"
  ]

  $is_puppet_apply = !(defined('$::servername') and $::servername)

  case "${::operatingsystem} ${::operatingsystemrelease}" {
         'Ubuntu 12.04': {
           $sssd_packages = ['sssd']
           $needs_nscd = true
         }
         default: {
           $sssd_packages = ['sssd', 'sssd-ldap']
           $needs_nscd = false
         }
  }

  # The files that should be deleted upon restarting sssd
  $sssd_cleanup_globs = "/var/lib/sss/db/*"

  if ($::os) { ## Puppet 3.8 or higher
      # There used to be an is_string function in stdlib, but at some
      # time around version 7.x it went out of fashion for some kind
      # of new-fangled type matching that for sure won't work in
      # Puppet 3.x, and probably will fall out of fashion again by the
      # time Puppet 9.x rolls out or whatever.
      $_os_is_string = inline_template('<%= (@os.instance_of? String) ? "yes" : "no" %>')
      if ($_os_is_string == "yes") {
        fail("\$::os being a string is not supported. Please install and execute the epflsti/quirks module.")
      }
      $osmajrelease = $::os["release"]["major"]
      $osfullrelease = $::os["release"]["full"]
      $osname = $::os["name"]
      $osfamily = $::os["family"]
  } else {
      $osmajrelease = $::operatingsystemmajrelease
      $osfullrelease = $::operatingsystemrelease
      $osname = $::operatingsystem
      $osfamily = $::osfamily
  }


  case $osfamily {
    'Debian': {
      $pam_modules_managed_by_distro = ["krb5", "mkhomedir", "sss", "winbind" ]
    }
  }

  $krb5_conf_file = $osfamily ? {
    "Darwin" => "/private/etc/krb5.conf",
    default  => "/etc/krb5.conf"
  }

  $is_dhcp = ($::networking and $::networking[dhcp])

  if (! $is_dhcp) {
    $ensure_gssapi_server = "fixed-ip"
  } elsif ($::domain == "intranet.epfl.ch") {
    $ensure_gssapi_server = "dhcp"
  } else {
    $ensure_gssapi_server = undef
  }

  $smb_conf_file = "/etc/samba/smb.conf"
  $manage_smb_conf = any2bool($::has_smb_conf)
  $manage_samba_secrets = any2bool($::has_smb_conf)
  $check_net_ads_testjoin = $manage_samba_secrets

  $ad_server_prod_base_dn = "DC=intranet,DC=epfl,DC=ch"
  $ad_server_test_base_dn = "DC=extest,DC=epfl,DC=ch"

  $defaults_nfs_common_path = "/etc/default/nfs-common"

  case $osfamily {
    "Debian": {
      $rpc_gssd_package = "nfs-common"
      $request_key_path = "/sbin/request-key"
      $nfsidmap_path = "/usr/sbin/nfsidmap"
      $request_key_package = "keyutils"
      $nfsidmap_package = "nfs-common"
      $ldap_conf_path = "/etc/ldap/ldap.conf"
    }
    "RedHat": {
      $rpc_gssd_package = "nfs-utils"
      $request_key_path = "/usr/sbin/request-key"
      $nfsidmap_path = "/usr/sbin/nfsidmap"
      $request_key_package = "keyutils"
      $nfsidmap_package = "nfs-utils"
      $ldap_conf_path = "/etc/openldap/ldap.conf"
    }
  }

  $autofs_deps = ["autofs"]
  $autofs_service = "autofs"

  case $osfamily {
    "RedHat": {
      $sshd_service = "sshd"
    }
    "Debian": {
      $sshd_service = "ssh"
    }
  }

  # Timeout for in-kernel Kerberos credentials (derived from your
  # /tmp/krb5cc_123456_something by rpc.gssd upon kernel request
  # aka "upcall").
  #
  # Since there is no way to invalidate these (see sections 6 and 7 of
  # http://www.citi.umich.edu/projects/nfsv4/linux/faq/), better put a
  # *very* conservative timeout on them - basically as low as we can
  # without hurting performance.
  $kernel_gss_lifetime_secs = 30

  # Timeout for an epfl_sso-specific kludge that delays starting automount
  # until it has its data ready (see ad_automount_home.pp)
  $ssd_automounts_available_timeout_seconds = 120
}
