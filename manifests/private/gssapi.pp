# Kerberos and GSSAPI configuration (client and server)
#
# === Parameters:
#
# $ensure:: Set to a true value to fail if the configuration cannot
#           support server-side GSSAPI (e.g. DHCP client or
#           incorrectly configured IP entry in /etc/hosts). Note that
#           enrolling in Active Directory is also a requirement, but
#           is *not* provided by this class (see epfl_sso::private::ad
#           instead)
#
# $use_test_realm:: If true, configure the test Active Directory realm instead of
#                   the production one.
#                   Default is false
#
# === Actions:
#
# * Create EPFL-compatible /etc/krb5.conf
#

class epfl_sso::private::gssapi(
  $ensure                = $::epfl_sso::private::params::ensure_gssapi_server,
  $ad_dns_domain         = $::epfl_sso::private::params::ad_dns_domain,
  $ad_server_rrdns       = $::epfl_sso::private::params::ad_server_rrdns,
  $use_test_realm        = false,
  $ad_test_dns_domain    = $::epfl_sso::private::params::ad_test_dns_domain,
  $ad_test_server        = $::epfl_sso::private::params::ad_test_dns_domain,
  $krb5_realm            = $::epfl_sso::private::params::krb5_realm,
  $krb5_test_realm       = $::epfl_sso::private::params::krb5_test_realm,
  $krb5_conf_file        = $::epfl_sso::private::params::krb5_conf_file,
) inherits epfl_sso::private::params {
  # In most circumstances, the host's FQDN is useless from a network
  # configuration standpoint. Unfortunately, Kerberos is the
  # exception. Whenever a Kerberized session needs to be established,
  # the client or server will request a session key from the KDC and
  # needs to know the exact identity of its communicating party in
  # order to do so successfully.
  if (($ensure == "fixed-ip") and (! $::fqdn_resolves_to_us)) {
    fail("Resolved IP address for ${::fqdn} is ${fqdn_ip}, which doesn't appear to be ours")
  }

  # There is no opting out of these checks.
  if ($::epfl_krb5_resolved == "false") {
    fail("Unable to resolve KDC in DNS - You must use the EPFL DNS servers.")
  }
  if ($::fqdn !~ /[.]/) {
    fail("Your FQDN isn't (${::fqdn}) - Refusing to create bogus AD entry")
  }

  $_krb5_default_realm = $use_test_realm ? {
    true    => $krb5_test_realm,
    default => $krb5_realm
  }

  augeas { "Default settings in ${krb5_conf_file}":
    context => "/files/${krb5_conf_file}",
    changes => [
      "set libdefaults/default_realm '${_krb5_default_realm}'",
      "set libdefaults/dns_lookup_realm false",
      "set libdefaults/dns_lookup_kdc false",
      "set libdefaults/rdns false"
    ]
  }

  augeas { "${krb5_realm} configuration in ${krb5_conf_file}":
    context => "/files/${krb5_conf_file}",
    changes => [
      "set realms/realm[1] ${krb5_realm}",
      "set realms/realm[1]/kdc[1] ${ad_server_rrdns}:88",
      "rm realms/realm[1]/kdc[5]",
      "rm realms/realm[1]/kdc[4]",
      "rm realms/realm[1]/kdc[3]",
      "rm realms/realm[1]/kdc[2]",
      "set realms/realm[1]/admin_server ${ad_server_rrdns}:749",
      "set realms/realm[1]/default_domain ${ad_dns_domain}",
    ],
  }

  if ($use_test_realm) {
    augeas { "${krb5_test_realm} configuration in ${krb5_conf_file}":
      context => "/files/${krb5_conf_file}",
      changes => [
        "set realms/realm[2] ${krb5_test_realm}",
        "set realms/realm[2]/kdc ${ad_test_server}:88",
        "set realms/realm[2]/admin_server ${ad_test_server}:749",
        "set realms/realm[2]/default_domain ${ad_test_dns_domain}",
      ],
    }
  }

  # Bugware: on Ubuntu 16.04, stock Augeas 1.4.0-0ubuntu1.1 (dragged in
  # by stock Puppet 3.8.5-2ubuntu0.1) cannot parse the stock krb5.conf
  # with all that MIT.EDU monkey business that the world needs to be aware
  # of it seems.
  if ("${::operatingsystem} ${::operatingsystemrelease}" == "Ubuntu 16.04") {
    exec { "rm ${krb5_conf_file}":
      path   => $::path,
      onlyif => "grep -q MIT.EDU ${krb5_conf_file}"
    } -> Augeas<| |>
  }

  define domain_goes_into_realm ($to_realm) {
    augeas { ".${title} in ${::epfl_sso::private::gssapi::krb5_conf_file}":
      context => "/files/${::epfl_sso::private::gssapi::krb5_conf_file}",
      changes => [
        "set domain_realm/.${title} ${to_realm}",
        "set domain_realm/${title} ${to_realm}"
      ]
    }
  }

  epfl_sso::private::gssapi::domain_goes_into_realm { $ad_dns_domain:
    to_realm => $krb5_realm
  }

  epfl_sso::private::gssapi::domain_goes_into_realm { [
    "epfl.ch",
    "iccluster.epfl.ch",
    "xaas.epfl.ch"
  ]:
    to_realm => $use_test_realm ? {
      true => $krb5_test_realm,
      default => $krb5_realm
    }
  }

  define ssh_auth_line() {
    file_line { "GSSAPIAuthentication 'yes' in ${title}":
      path => $title,
      line => "GSSAPIAuthentication yes",
      match => "GSSAPIAuthentication",
      ensure => "present",
      multiple => true
    }
  }

  # Turn on pam_krb5 where possible (not in RH / CentOS 7.9 or 8; see
  # https://access.redhat.com/solutions/4219971) for the purpose of
  # creating tickets for people who log in via password:
  include epfl_sso::private::pam
  unless ($osfamily == "RedHat" and versioncmp($osfullrelease, "7.9") >= 0) {
    epfl_sso::private::pam::module { "krb5": }
  }
}
